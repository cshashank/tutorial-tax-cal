class ItemData
	def initialize (country,price)
		@country=country
		@price=price	
	end

	def give_country
		@country
	end

	def give_price
		@price
	end
	def tax_usa
		(TaxUsa.new@price).tax_cal_usa
	end
	def tax_uk
		(TaxUk.new@price).tax_cal_uk
	end

	def tax_india
		(TaxIndia.new@price).tax_cal_india
	end
end